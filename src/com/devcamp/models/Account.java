package com.devcamp.models;

public class Account {
 /** 
  * Thuộc tính
  */
 private int id;
 private Customer customer;
 private double balance = 0.0;
 /**
 * Phương thức
 *
 */

 public Account(int id, Customer customer, double balance) {
  this.id = id;
  this.customer = customer;
  this.balance = balance;
}

public Account(int id, Customer customer) {
 this.id = id;
 this.customer = customer;
}
/**
 * Getter method
 * @return
 */
public int getId() {
 return id;
}

public Customer getCustomer() {
 return customer;
}

public double getBalance() {
 return balance;
}

/**
 * Settter method
 * @param
 */
public void setBalance(double balance) {
 this.balance = balance;
}

public String getCustomerName() {
 return customer.getName();
}

public Account deposit( Double amount) {
  this.balance += amount;
  return this;
}

public Account withdraw( Double amount) {
 if (this.balance >= amount) {
  this.balance -= amount;
 }
 else {
  System.out.println("amount withdrawn exceeds the current balance!");
 }
 return this;
}

@Override
public String toString() {
 double roundoff = (double) Math.round(balance *100)/100;
 return String.format("Name (id = %s) balance = $%s",id,  roundoff);
}
}
