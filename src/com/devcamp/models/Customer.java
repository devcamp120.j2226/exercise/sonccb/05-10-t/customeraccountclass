package com.devcamp.models;

public class Customer {
 private int id;
 private String name;
 private int discount;

 public Customer(int id, String name, int discount) {
  /**
   * Phương thức
   */
   this.id = id;
   this.name = name;
   this.discount = discount;

   /**
    * Getter method
    * @return
    */

   }

   public Customer(int id2) {
 }

   public int getId(){
    return id;
   }

   public String getName() {
    return name;
   }

   public int getDiscount() {
    return discount;
   }
   /**
    * Setter method
    * @param
    */

    public void setDiscount(int discount) {
     this.discount = discount;
    }

    @Override
    public String toString() {
     return name +"(" + id + ")"+ "(" + discount + "%)";
    }

}
